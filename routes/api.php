<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/getS3/{bucket}/{folder?}','HomeController@getSignature' );

Route::get('/deleteDirectory/{bucket}','HomeController@deleteDirectory' );

Route::get('/makeDirectory/{bucket}','HomeController@makeDirectory' );

Route::get('/getDirectoriesList/{bucket}','HomeController@listDirectories' );

Route::get('/test','HomeController@test' );
