<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class HomeController extends Controller
{
    public function getSignature($bucket , $folder=null,Request $request)
    {
        $folder=$request->path;
        if($bucket == 'disclosuretutor')
        {
              $s3 = \Storage::disk('disclosuretutor');
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'invalid bucket name'));
        }
        if($folder == null)
        {
            $folder='/';
        }
        $files = $s3->files($folder);

        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+20 minutes";
        $response=array();
        foreach($files as $file)
        {

            $command = $client->getCommand('deleteObject', [
                'Bucket' => $bucket,
                'Key'    => $file,
            ]);
            $request = $client->createPresignedRequest($command, $expiry);
            ob_start(); //Start output buffer
            echo $request->getUri();
            $output = ob_get_contents(); //Grab output
            ob_end_clean(); //Discard output buffer
            $temp_delete=$output   ;



            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $file,
            ]);
            $request = $client->createPresignedRequest($command, $expiry);
            ob_start(); //Start output buffer
            echo $request->getUri();
            $output = ob_get_contents(); //Grab output
            ob_end_clean(); //Discard output buffer
            $temp=$output   ;


            $response[basename($file)]=
                array('path' =>$file,
                    'readApi'=>$temp,
                    'deleteApi'=>$temp_delete
                );
        }

            return response($response);

    }

    public function listDirectories($bucket)
    {
        if($bucket == 'disclosuretutor')
        {
           $s3 = \Storage::disk('disclosuretutor');
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'invalid bucket name'));
        }
        return response($s3->allDirectories('/'));

    }
    public function makeDirectory($bucket,Request $request)
    {
        $path=$request->path;
        if($bucket == 'disclosuretutor')
        {
           $s3 = \Storage::disk('disclosuretutor');
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'invalid bucket name'));
        }
        if($path == null)
        {
            return response(array('status' => 'false',                            
                                   'message' => 'Please provide directory path'));
        }
        $result=$s3->makeDirectory($path);
        if($result == true)
        {
            return response(array('status' => 'true',
                                   'message' => 'Directory has been created successfully'));
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'Something went wrong. Please try again.'));
        }

    }

    public function deleteDirectory($bucket,Request $request)
    {
        $path=$request->path;
        if($bucket == 'disclosuretutor')
        {
           $s3 = \Storage::disk('disclosuretutor');
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'invalid bucket name'));
        }
        if($path == null)
        {
            return response(array('status' => 'false',
                                   'message' => 'Please provide directory path'));
        }
        $result=$s3->deleteDirectory($path);
        if($result == true)                                                                         
        {
            return response(array('status' => 'true',
                                   'message' => 'Directory has been created successfully'));
        }
        else
        {
            return response(array('status' => 'false',
                                   'message' => 'Something went wrong. Please try again.'));
        }

    }

    public function test()
    {
        //            $stream = Storage::disk('s3')->getDriver()->readStream($template->url);
//        $t=Storage::disk('s3')->delete('test1.jpeg');
//        $t=Storage::disk('s3')->deleteDirectory('testDirectory/test');
//        dd($t);
//        $files = Storage::disk('s3')->files('images');
////        dd($files);
        $s3 = \Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+20 minutes";
        $command = $client->getCommand('deleteObject', [
            'Bucket' => 'disclosuretutor',
            'Key'    => 'test1.jpeg',
        ]);
        $request = $client->createPresignedRequest($command, $expiry);
        ob_start(); //Start output buffer
        echo $request->getUri();
        $output = ob_get_contents(); //Grab output
        ob_end_clean(); //Discard output buffer
         $response=array();
        $temp=$output   ;
            return response($temp);

    }
}
